# Mineswepeers

Test Desarrollador front-end

## Instalación

ejecutar npm install para descargar las dependencias.

```bash
npm install
```
Ejecutar unit test.

```bash
ng test
```

Ejecutar app desarrollo

```bash
ng serve
```

## Paquetes instalados

```
- Bootstrap.
- FontAwesome.
- Poppover.js.
- Jquery.js.
```
- Se instalaron estas librerías para utilizar los estilos de bootstrap que requieren poppover y jquery.
- Se instalo FontAwesome para utilizar los iconos.

## Instrucciones del juego :
 - La app se levante apuntando a la ruta por defecto /home, donde se puede encontrar un botón para entrar al juego.
- El juego cuenta con 3 niveles de dificultad : Principiante , Intermedio , Experto.
- Se puede seleccionar la dificultad en el dropdown de dificultad y automáticamente el tablero se actualiza.
- El tiempo empieza a correr cuando se descubre la primer celda.
- Se guarda temporalmente en una tabla los datos cuando se finaliza una partida.
- El botón empezar de nuevo resetea todo el juego.
 
### Explicación de niveles
- Nivel Principiante: 8 × 8 casillas y 5 minas.
- Nivel Intermedio: 16 x 16 casillas y 36 minas.
- Nivel Experto: 20 x 20 y 40 minas.