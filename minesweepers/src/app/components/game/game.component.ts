import { Component, OnInit } from "@angular/core";
import { Board } from "../../models/board";
import { Cell } from "../../models/cell";
import { ResultGames } from "../../models/resultsGames";

import { trigger, style, animate, transition } from "@angular/animations";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.css"],
  animations: [
    trigger("fade", [
      transition("void => *", [
        style({ opacity: 0 }),
        animate(2000, style({ opacity: 1 })),
      ]),
    ]),
  ],
})
export class GameComponent implements OnInit {
  niveles = [
    { name: "Principiante" },
    { name: "Intermedio" },
    { name: "Experto" },
  ];
  resultadosJuego: ResultGames[] = [];
  board: Board;
  contador: number = 0;
  gameover: boolean = false;
  victoria: boolean = false;
  juegoTerminado: boolean = false;
  opcionSeleccionado: string = "Principiante";
  verSeleccion: string = "";
  casillas: number = 0;
  minas: number = 0;
  textoAlert: string = "";
  time: number = 0;
  interval;
  play: boolean = false;
  bloquearStartTimer: boolean = false;

  constructor() {}

  ngOnInit() {
    this.EmpezarJuegoDificultadSeleccionada();
  }

  startTimer() {
    if (this.bloquearStartTimer === false) {
      this.play = true;
      this.interval = setInterval(() => {
        this.time++;
      }, 1000);
    }
  }

  pauseTimer() {
    this.play = false;
    this.bloquearStartTimer = false;
    this.time = 0;
    clearInterval(this.interval);
  }

  obtenerDificultadIndexCbo(dificultad) {
    this.opcionSeleccionado = dificultad;
    this.dificultadJuego(dificultad);
  }

  verificarEstadoCelda(cell: Cell) {
    this.startTimer();
    const result = this.board.verificarEstadoCelda(cell);
    this.bloquearStartTimer = true;
    this.verificarEstadoJuego(result);
  }

  verificarEstadoJuego(estado: string) {
    if (this.isGameOver(estado)) {
      this.cargarDatosGameOver();
      this.pauseTimer();
    } else if (this.isWin(estado)) {
      this.cargarDatosWin();
      this.pauseTimer();
    }
  }

  cargarDatosGameOver() {
    this.victoria = false;
    this.gameover = true;
    this.juegoTerminado = true;
    this.textoAlert = "Perdiste";
    this.resultadosJuego.push(new ResultGames(this.contador,this.time,this.textoAlert,this.opcionSeleccionado));
    this.contador = 0;
  }

  cargarDatosWin() {
    this.victoria = true;
    this.gameover = false;
    this.juegoTerminado = true;
    this.textoAlert = "Ganaste";
    this.resultadosJuego.push(new ResultGames(this.contador,this.time,this.textoAlert,this.opcionSeleccionado));
  }

  isGameOver(resultado: string): boolean {
    return resultado === "gameover" ? true : false;
  }

  isWin(resultado: string): boolean {
    return resultado === "win" ? true : false;
  }

  colocarFlag(cell: Cell) {
    if (cell.status === "flag") {
      cell.status = "open";
      this.contador--;
    } else {
      cell.status = "flag";
      this.contador++;
    }
  }

  EmpezarJuego(cell: number, mine: number) {
    this.pauseTimer();
    this.board = new Board();
    this.board.empezarJuego(cell, mine);
    this.victoria = false;
    this.gameover = false;
    this.juegoTerminado = false;
    this.contador = 0;
  }

  EmpezarJuegoDificultadSeleccionada() {
    this.dificultadJuego(this.opcionSeleccionado);
  }

  dificultadJuego(dificultad: string) {
    if (dificultad === "Principiante") {
      this.EmpezarJuego(8, 5);
    } else if (dificultad === "Intermedio") {
      this.EmpezarJuego(16, 40);
    } else if (dificultad === "Experto") {
      this.EmpezarJuego(20, 40);
    }
  }
}
