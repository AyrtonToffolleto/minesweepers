import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from "./game.component";

describe("GameComponent", () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let resultdado: string;

  const routes: Routes = [
    { path: "game", component: GameComponent },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameComponent],
      imports: [
        BrowserAnimationsModule,
        RouterModule.forRoot(routes)
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("when result is gameover return true", () => {
    resultdado = "gameover";
    expect(component.isGameOver(resultdado)).toBe(true);
  });

  it("when result is win return true", () => {
    const fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    resultdado = "win";
    expect(component.isWin(resultdado)).toBe(true);
  });

  it("when result not is win return false", () => {
    const fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    resultdado = "gameover";
    expect(component.isWin(resultdado)).toBe(false);
  });

  it("when result not is gameover return false", () => {
    const fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    resultdado = "win";
    expect(component.isGameOver(resultdado)).toBe(false);
  });

  it("when start game", () => {
    const fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    resultdado = "win";
    expect(component.EmpezarJuego(10,8));
  });
});
