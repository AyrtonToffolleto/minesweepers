import { Cell } from "./cell";

const COMBINACIONES = [
  [-1, -1],
  [-1, 0],
  [-1, 1],
  [0, -1],
  [0, 1],
  [1, -1],
  [1, 0],
  [1, 1],
];

export class Board {
  cells: Cell[][] = [];
  private mineCount = 0;
  private celdasRestantes = 0;

  constructor() {}

  empezarJuego(size: number, mines: number) {
    this.crearTablero(size);
    this.agregarMinas(size, mines);
  }

  crearTablero(size: number) {
    for (let y = 0; y < size; y++) {
      this.cells[y] = [];
      for (let x = 0; x < size; x++) {
        this.cells[y][x] = new Cell(y, x);
      }
    }
  }

  agregarMinas(size: number, mines: number) {
    for (let y = 0; y < mines; y++) {
      this.generarMinasRandomEnCelda().mine = true;
    }

    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        let minasAdjacentes = 0;
        for (const combinacion of COMBINACIONES) {
          if (
            this.cells[y + combinacion[0]] &&
            this.cells[y + combinacion[0]][x + combinacion[1]] &&
            this.cells[y + combinacion[0]][x + combinacion[1]].mine
          ) {
            minasAdjacentes++;
          }
        }
        this.cells[y][x].proximityMines = minasAdjacentes;

        if (this.cells[y][x].mine) {
          this.mineCount++;
        }
      }
    }
    this.celdasRestantes = size * size - this.mineCount;
  }

  generarMinasRandomEnCelda(): Cell {
    const y = Math.floor(Math.random() * this.cells.length);
    const x = Math.floor(Math.random() * this.cells[y].length);
    return this.cells[y][x];
  }

  verificarEstadoCelda(cell: Cell): "gameover" | "win" | null {
    if (cell.status !== "open") {
      return;
    } else if (cell.mine) {
      this.revealAll();
      return "gameover";
    } else {
      cell.status = "clear";

      if (cell.proximityMines === 0) {
        for (const combinacion of COMBINACIONES) {
          if (
            this.cells[cell.row + combinacion[0]] &&
            this.cells[cell.row + combinacion[0]][cell.column + combinacion[1]]
          ) {
            this.verificarEstadoCelda(
              this.cells[cell.row + combinacion[0]][cell.column + combinacion[1]]
            );
          }
        }
      }

      if (this.celdasRestantes-- <= 1) {
        return "win";
      }
      return;
    }
  }

  revealAll() {
    for (const row of this.cells) {
      for (const cell of row) {
        if (cell.status === "open") {
          cell.status = "clear";
        }
      }
    }
  }
}
