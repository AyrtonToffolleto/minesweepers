export class ResultGames {
  flagsUse: number;
  timeUse: number;
  statusGame: string;
  difficulty: string;

  constructor(flagsUse: number, timeUse: number, statusGame: string,difficulty: string) {
    this.flagsUse = flagsUse;
    this.timeUse = timeUse;
    this.statusGame = statusGame;
    this.difficulty = difficulty;
  }
}
